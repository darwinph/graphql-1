defmodule Graphical.UserResolver do
  alias Graphical.Accounts

  def all(_args, _info) do
    {:ok, Accounts.list_users()}
  end

  def find(%{id: id}, _info) do
    case Accounts.get_user!(id) do
      nil -> {:error, "User is not found"}
      user -> {:ok, user}
    end
  end

  def create_user(args, _info) do
    case Accounts.create_user(args) do
      {:ok, user} ->
        {:ok, user}

      _error ->
        {:error, "Could not create user"}
    end
  end

  def delete_user(%{id: id}, _info) do
    Accounts.get_user!(id)
    |> Accounts.delete_user()
  end
end
