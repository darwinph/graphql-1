defmodule GraphicalWeb.Schema do
  use Absinthe.Schema
  import_types(GraphicalWeb.Schema.Types)

  query do
    field :get_all_posts, list_of(:post) do
      resolve(&Graphical.PostResolver.all/2)
    end

    field :get_all_users, list_of(:user) do
      resolve(&Graphical.UserResolver.all/2)
    end

    field :find_user, type: :user do
      arg(:id, non_null(:id))
      resolve(&Graphical.UserResolver.find/2)
    end
  end

  input_object :update_post_params do
    field(:title, non_null(:string))
    field(:body, non_null(:string))
    field(:user_id, non_null(:integer))
  end

  mutation do
    field :create_user, :user do
      arg(:name, non_null(:string))
      arg(:email, non_null(:string))

      resolve(&Graphical.UserResolver.create_user/2)
    end

    field :update_post, :post do
      arg(:id, non_null(:integer))
      arg(:post, :update_post_params)

      resolve(&Graphical.PostResolver.update/2)
    end

    field :delete_user, :user do
      arg(:id, non_null(:integer))

      resolve(&Graphical.UserResolver.delete_user/2)
    end
  end
end
