defmodule GraphicalWeb.Router do
  use GraphicalWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/" do
    pipe_through(:api)

    forward("/graphiql", Absinthe.Plug.GraphiQL,
      schema: GraphicalWeb.Schema,
      interface: :simple,
      context: %{pubsub: GraphicalWeb.Endpoint}
    )

    forward("/", Absinthe.Plug, schema: GraphicalWeb.Schema)
  end
end
