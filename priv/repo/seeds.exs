# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Graphical.Repo.insert!(%Graphical.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Graphical.Accounts.User
alias Graphical.Posts.Post
alias Graphical.Repo

# %User{name: "Darwin Manalo", email: "darwin@email.com"} |> Repo.insert!()
# %User{name: "Julius Dejon", email: "julius@email.com"} |> Repo.insert!()

# %Post{title: Faker.Lorem.sentence(), body: Faker.Lorem.paragraph(), user_id: 1} |> Repo.insert!()
# %Post{title: Faker.Lorem.sentence(), body: Faker.Lorem.paragraph(), user_id: 2} |> Repo.insert!()
# %Post{title: Faker.Lorem.sentence(), body: Faker.Lorem.paragraph(), user_id: 1} |> Repo.insert!()
# %Post{title: Faker.Lorem.sentence(), body: Faker.Lorem.paragraph(), user_id: 2} |> Repo.insert!()
# %Post{title: Faker.Lorem.sentence(), body: Faker.Lorem.paragraph(), user_id: 1} |> Repo.insert!()

for _ <- 1..10 do
  %Post{
    title: Faker.Lorem.sentence(),
    body: Faker.Lorem.paragraph(),
    user_id: Enum.random([1, 2])
  }
  |> Repo.insert!()
end
